package sampletdd2;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {

	
//R1 divive by 3
	@Test
	public void testDivisibleBy3() {
		
		FizzBuzz b = new FizzBuzz();
		String result = b.buzz(27);
		assertEquals("Fizz", result);
	}
	//R2 divide by 5
	@Test
	public void testDivisibleBy5() {
		
		FizzBuzz b = new FizzBuzz();
		String result = b.buzz(10);
		assertEquals("Buzz", result);
	}

	//R3 divide by 15
		@Test
		public void testDivisibleBy5and3() {
			
			FizzBuzz b = new FizzBuzz();
			String result = b.buzz(15);
			assertEquals("FizzBuzz", result);
		}
		@Test
		public void testOtherNumber() {
			
			FizzBuzz b = new FizzBuzz();
			String result = b.buzz(4);
			assertEquals("4", result);
		}
		
		@Test
		public void testprime() {
			
			FizzBuzz b = new FizzBuzz();
			String result = b.buzz(11);
			assertEquals("whizz", result);
		}
		@Test
		public void testAppendWhizz() {
			
			FizzBuzz b = new FizzBuzz();
			
			String result = b.buzz(3);
			assertEquals("fizzwhizz", result);
		
			result = b.buzz(5);
			assertEquals("buzzwhizz", result);
		
		}
		
}
